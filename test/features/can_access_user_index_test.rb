require "test_helper"

class CanAccessUserIndexTest < Capybara::Rails::TestCase
  test "index page test" do
    visit users_path
    assert page.must_have_css('table')
    within('table') do
      users.each do |user|
        selector = '#user-#{user.id}'
        page.must_have_css(selector)
        within(selector) do
          page.has_content user.name
          page.has_content user.email
        end
      end
    end
    assert page.has_button?('New')
    refute_content page, "Goobye All!"
  end
end
