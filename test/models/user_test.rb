require "test_helper"

class UserTest < ActiveSupport::TestCase


  def user
    #puts 'Test Running on ####  ' + Figaro.env.APP_DB_DATABASE  + ' ####';
    @user = users(:one)
  end

  def test_for_empty_name
    user.name = nil
    assert_not user.valid?
  end

  def test_for_name_length
    user.name = 'abc'
    assert_not user.valid?
  end

  def test_for_empty_email
    user.email = nil
    assert_not user.valid?
  end

  def test_for_case_sensitive_email
    user1 = users(:one)
    user2 = users(:two)
    user2.email = user1.email.upcase
    user2.save
    assert_not user2.valid?
  end

end
