require "test_helper"

class UsersControllerTest < ActionController::TestCase
  def test_index
    get :index
    assert_not_nil assigns(:users)
    assert_select 'table'
  end

  def test_create_user
    assert_difference('User.count') do
      post :create, user: {name: 'test1', email:'test@test.com'}
    end
    assert_redirected_to users_path
  end

  def test_new_user
    get :new
    assert_response :success
    assert_select 'form'
  end

end
